﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Client;

namespace Node
{
    internal class AssemblyLoader<T> where T : class, IGraph
    {
        static AssemblyLoader()
        {
            var appDomain = AppDomain.CurrentDomain;

            appDomain.AssemblyResolve += (sender, args) =>
            {
                var domain = AppDomain.CurrentDomain;
                var assemblies = domain.GetAssemblies();
                var asm = assemblies.FirstOrDefault(p => p.FullName == args.Name);
                return asm;
            };
        }

        public T Load(IEnumerable<string> files, string workingDirectory)
        {
            T graph = null;
            
            foreach (var file in files)
            {
                var asm = Assembly.LoadFile(file);

                var type = asm.GetExportedTypes().FirstOrDefault(p => p.IsClass && typeof(T).IsAssignableFrom(p));

                if (type != null)
                {
                    graph = (T) Activator.CreateInstance(type);
                    graph.Directory = workingDirectory;
                }
            }
            
            return graph;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Client;
using Microsoft.AspNet.SignalR.Client;
using StackExchange.Redis;

namespace Node
{
    class MinimumSpanningTree : IAlgorithm
    {
        private const string _mstHubName = "MinimumSpanningTreeHub";
        private readonly TextWriter _logWriter;
        private readonly IDatabase _database;
        private IHubProxy _mstProxy;
        private readonly AssemblyLoader<IMinimumSpanningTree> _loader;
        private IMinimumSpanningTree _mst;
        private long _vertexFromOut;
        private long _vertexToOut;
        private long _vertexFromIn;
        private long _vertexToIn;
        private Dictionary<long, List<Edge>> _graph;
        private List<Edge> _includedEdges;
        private Dictionary<long, bool> _includedVertices;
        private long _root;
        private bool _success;
        private string _abortMessage;
        private AutoResetEvent _event;
        private bool _abort;

        public MinimumSpanningTree(ConnectionMultiplexer multiplexer, TextWriter logWriter)
        {
            _database = multiplexer.GetDatabase();
            _logWriter = logWriter;
            _loader = new AssemblyLoader<IMinimumSpanningTree>();
        }

        public void RegisterProxy(HubConnection connection)
        {
            _mstProxy = connection.CreateHubProxy(_mstHubName);

            RegisterEvents();
        }

        public Task<Tuple<bool, string>> Compute(string workingDirectory, long vertexFromOut, long vertexToOut, long vertexFromIn, long vertexToIn, IEnumerable<string> files, bool restore)
        {
            _vertexFromOut = vertexFromOut;
            _vertexFromIn = vertexFromIn;
            _vertexToOut = vertexToOut;
            _vertexToIn = vertexToIn;
            _success = true;
            _abortMessage = null;
            _event = new AutoResetEvent(false);
            _abort = false;

            var task = new Task<Tuple<bool, string>>(() =>
            {
                _event.WaitOne();
                var res = new Tuple<bool, string>(_success, _abortMessage);
                Clean();
                return res;
            });

            task.Start();

            IEnumerable<Edge> tmpGraph = null;
            try
            {
                _mst = _loader.Load(files, workingDirectory);
                if (_mst == null)
                    throw new Exception("Required type not implemented");
                _root = _mst.Root;
                tmpGraph = _mst.LoadGraph(_vertexFromOut, _vertexToOut, _vertexFromIn, _vertexToIn);
                tmpGraph = tmpGraph ?? new Edge[0];
            }
            catch (Exception ex)
            {
                _success = false;
                _abortMessage = ex.Message;
                SendFailure();
                return task;
            }

            try
            {
                Initialize(tmpGraph);
                if (tmpGraph.LongCount() == 0)
                {
                    SendSuccess();
                    return task;
                }
                tmpGraph = null;
                if (restore)
                    Restore();
                Compute();
            }
            catch (Exception ex)
            {
                _success = false;
                _abortMessage = ex.Message;
            }

            return task;
        }

        public void Abort()
        {
            _mstProxy.Invoke("CleanAll");
            _abort = true;
            _event.Set();
        }

        private void RegisterEvents()
        {
            _mstProxy.On("UpdateGraph", (long vertexFrom, long vertexTo, double weight) =>
            {
                try
                {
                    UpdateGraph(vertexFrom, vertexTo);
                    Compute();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                }
            });

            _mstProxy.On("SaveResults", () =>
            {
                try
                {
                    SaveResults();
                    _success = true;
                    _event.Set();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                    _event.Set();
                }
            });
        }

        private void Initialize(IEnumerable<Edge> edges)
        {
            _graph = new Dictionary<long, List<Edge>>();
            _includedEdges = new List<Edge>();
            _includedVertices = new Dictionary<long, bool>();

            foreach (var edge in edges)
            {
                if(edge == null)
                    continue;

                if (!_graph.ContainsKey(edge.VertexFrom))
                    _graph.Add(edge.VertexFrom, new List<Edge>());

                _graph[edge.VertexFrom].Add(edge);

                if (!_includedVertices.ContainsKey(edge.VertexFrom))
                    _includedVertices.Add(edge.VertexFrom, false);

                if (!_includedVertices.ContainsKey(edge.VertexTo))
                    _includedVertices.Add(edge.VertexTo, false);
            }

            if (_includedVertices.ContainsKey(_root))
                _includedVertices[_root] = true;
        }

        private void Restore()
        {
            var entries = _database.HashGetAll("MstResults").ToList();

            foreach (var entry in entries)
                UpdateGraph((long)entry.Value, (long)entry.Name);
        }

        private void Compute()
        {
            var edge = FindMin();

            if (edge != null)
                SendMin(edge.VertexFrom, edge.VertexTo, edge.Weight);
            else
                SendSuccess();
        }

        private Edge FindMin()
        {
            var minEdge = new Edge(-1, -1, double.MaxValue);

            if (_includedVertices.Count(p => p.Value) == _includedVertices.Count)
                return null;

            foreach (var key in _graph.Keys)
                if (_includedVertices[key])
                    foreach (var edge in _graph[key])
                        if (!_includedVertices[edge.VertexTo] && edge.Weight <= minEdge.Weight)
                            minEdge = edge;

            return minEdge;
        }

        private void UpdateGraph(long vertexFrom, long vertexTo)
        {
            if (_includedVertices.ContainsKey(vertexTo))
            {
                _includedVertices[vertexTo] = true;

                if (_graph.ContainsKey(vertexFrom))
                {
                    var edge = _graph[vertexFrom].Find(p => p.VertexTo == vertexTo);
                    
                    if(edge != null)
                        _includedEdges.Add(edge);
                }
            }
        }

        private void SendMin(long vertexFrom, long vertexTo, double weight)
        {
            _mstProxy.Invoke("UpdateMin", vertexFrom, vertexTo, weight);
        }

        private void SendSuccess()
        {
            _mstProxy.Invoke("Result", true);
        }

        private void SendFailure()
        {
            _mstProxy.Invoke("Result", false);
        }

        private void SaveResults()
        {
            _mst.SaveResults(_includedEdges);
        }

        private void Clean()
        {
            _mst = null;
            _graph = null;
            _includedEdges = null;
            _includedVertices = null;
        }
    }
}

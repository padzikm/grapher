﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Node
{
    static class Program
    {
        private static void Main(string[] args)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var path2 = Path.GetDirectoryName(path);
            path2 = @path2 + @"\Data\";
            Directory.CreateDirectory(path2);
            var cats = Directory.EnumerateDirectories(@path2);
            var numbers = cats.Select(p => int.Parse(Path.GetFileName(p)));
            var dir2 = cats.Any() ? (numbers.Max() + 1).ToString() : "0";
            var currentDirectory = @path2 + dir2;
            Directory.CreateDirectory(currentDirectory);

            var node = new Node(currentDirectory, Console.Out);

            var connectionId = node.Start();

            if (connectionId == null)
            {
                Console.WriteLine("Registered failed\n");
                Console.WriteLine("Exiting\n");
                node.Stop();

                return;
            }

            Console.WriteLine("\nRegistered successfully\n");

            Console.WriteLine("ConnectionId: {0}\n", connectionId);

            Console.WriteLine("Type 'exit' to finish program\n");

            var line = Console.ReadLine();

            while (line.ToLower() != "exit")
                line = Console.ReadLine();

            node.Stop();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SevenZip;

namespace Node
{
    class ZipExtractor
    {
        private const string _archiveName = "archive.7z";

        public IEnumerable<string> Extract(string directory, byte[] tab)
        {
            var fileList = new List<string>();
            var path3 = directory + _archiveName;

            File.WriteAllBytes(@path3, tab);

            using (var tmp = new SevenZipExtractor(@path3))
            {
                for (int i = 0; i < tmp.ArchiveFileData.Count; i++)
                {
                    tmp.ExtractFiles(directory, tmp.ArchiveFileData[i].FileName);
                }

                fileList = tmp.ArchiveFileNames.Where(p => p.EndsWith(".dll"))
                        .Select(p => directory + @"\" + p)
                        .ToList();
            }

            return fileList;
        }
    }
}

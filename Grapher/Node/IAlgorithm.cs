﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;

namespace Node
{
    interface IAlgorithm
    {
        void RegisterProxy(HubConnection connection);
        Task<Tuple<bool, string>>  Compute(string workingDirectory, long vertexFromOut, long vertexToOut, long vertexFromIn, long vertexToIn, IEnumerable<string> files, bool restore);
        void Abort();
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Client;
using Microsoft.AspNet.SignalR.Client;
using StackExchange.Redis;

namespace Node
{
    class Dijkstra : IAlgorithm
    {
        private const string _dijkstraHubName = "DijkstraHub";
        private readonly TextWriter _logWriter;
        private readonly IDatabase _database;
        private IHubProxy _dijkstraProxy;
        private readonly AssemblyLoader<IDijkstra> _loader;
        private IDijkstra _dijkstra;
        private long _vertexFromOut;
        private long _vertexToOut;
        private long _vertexFromIn;
        private long _vertexToIn;
        private Dictionary<long, List<Edge>> _graph;
        private Dictionary<long, double?> _shortestPaths;
        private Dictionary<long, bool> _includedVertices;
        private long _root;
        private long? _destination;
        private bool _success;
        private string _abortMessage;
        private AutoResetEvent _event;
        private bool _abort;

        public Dijkstra(ConnectionMultiplexer multiplexer, TextWriter logWriter)
        {
            _database = multiplexer.GetDatabase();
            _logWriter = logWriter;
            _loader = new AssemblyLoader<IDijkstra>();
        }

        public void RegisterProxy(HubConnection connection)
        {
            _dijkstraProxy = connection.CreateHubProxy(_dijkstraHubName);

            RegisterEvents();
        }

        public Task<Tuple<bool, string>> Compute(string workingDirectory, long vertexFromOut, long vertexToOut, long vertexFromIn, long vertexToIn, IEnumerable<string> files, bool restore)
        {
            _vertexFromOut = vertexFromOut;
            _vertexFromIn = vertexFromIn;
            _vertexToOut = vertexToOut;
            _vertexToIn = vertexToIn;
            _success = true;
            _abortMessage = null;
            _event = new AutoResetEvent(false);
            _abort = false;

            var task = new Task<Tuple<bool, string>>(() =>
            {
                _event.WaitOne();
                var res = new Tuple<bool, string>(_success, _abortMessage);
                Clean();
                return res;
            });

            task.Start();

            IEnumerable<Edge> tmpGraph = null;
            try
            {
                _dijkstra = _loader.Load(files, workingDirectory);
                if (_dijkstra == null)
                    throw new Exception("Required type not implemented");
                _root = _dijkstra.Root;
                _destination = _dijkstra.Destination;
                tmpGraph = _dijkstra.LoadGraph(_vertexFromOut, _vertexToOut, _vertexFromIn, _vertexToIn);
                tmpGraph = tmpGraph ?? new Edge[0];
            }
            catch (Exception ex)
            {
                _abortMessage = ex.Message;
                SendFailure();
                return task;
            }

            try
            {
                Initialize(tmpGraph);
                if (tmpGraph.LongCount() == 0 || (_destination != null && _root == _destination))
                {
                    SendSuccess();
                    return task;
                }
                tmpGraph = null;
                if (restore)
                    Restore();
                Compute();
            }
            catch (Exception ex)
            {
                _success = false;
                _abortMessage = ex.Message;
            }

            return task;
        }

        public void Abort()
        {
            _dijkstraProxy.Invoke("CleanAll");
            _abort = true;
            _event.Set();
        }

        private void RegisterEvents()
        {
            _dijkstraProxy.On("UpdateGraph", (long vertex, long route) =>
            {
                try
                {
                    UpdateGraph(vertex, route);
                    if (_destination != null && _destination == vertex)
                    {
                        SendSuccess();
                        return;
                    }
                    Compute();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                }
            });

            _dijkstraProxy.On("SaveResults", () =>
            {
                try
                {
                    SaveResults();
                    _success = true;
                    _event.Set();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                    _event.Set();
                }
            });
        }

        private void Initialize(IEnumerable<Edge> edges)
        {
            _graph = new Dictionary<long, List<Edge>>();
            _shortestPaths = new Dictionary<long, double?>();
            _includedVertices = new Dictionary<long, bool>();

            foreach (var edge in edges)
            {
                if(edge == null)
                    continue;

                if (!_graph.ContainsKey(edge.VertexFrom))
                    _graph.Add(edge.VertexFrom, new List<Edge>());

                _graph[edge.VertexFrom].Add(edge);

                if (!_shortestPaths.ContainsKey(edge.VertexFrom))
                    _shortestPaths.Add(edge.VertexFrom, null);

                if (!_shortestPaths.ContainsKey(edge.VertexTo))
                    _shortestPaths.Add(edge.VertexTo, null);

                if (!_includedVertices.ContainsKey(edge.VertexFrom))
                    _includedVertices.Add(edge.VertexFrom, false);

                if (!_includedVertices.ContainsKey(edge.VertexTo))
                    _includedVertices.Add(edge.VertexTo, false);

                if (edge.VertexFrom == _root)
                    _shortestPaths[edge.VertexTo] = edge.Weight;
            }

            if (_includedVertices.ContainsKey(_root))
            {
                _shortestPaths[_root] = 0;
                _includedVertices[_root] = true;
            }
        }

        private void Restore()
        {
            var entries = _database.HashGetAll("DijkstraResults").ToList();

            foreach (var entry in entries)
                UpdateGraph((long)entry.Name, (double)entry.Value);
        }

        private void Compute()
        {
            var edge = FindMin();

            if (edge != null)
                SendMin(edge.Item1, edge.Item2, edge.Item3);
            else
                SendSuccess();
        }

        private Tuple<long, long, double> FindMin()
        {
            long from = -1, to = -1;
            double route = double.MaxValue;

            if (_includedVertices.Count(p => p.Value) == _includedVertices.Count)
                return null;

            foreach (var key in _graph.Keys)
                if (_includedVertices[key])
                    foreach (var edge in _graph[key])
                        if (!_includedVertices[edge.VertexTo] && _shortestPaths[edge.VertexTo] <= route)
                        {
                            from = key;
                            to = edge.VertexTo;
                            route = _shortestPaths[to].Value;
                        }

            return new Tuple<long, long, double>(from, to, route);
        }

        private void UpdateGraph(long vertex, double route)
        {
            if (_includedVertices.ContainsKey(vertex))
                _includedVertices[vertex] = true;

            if (_shortestPaths.ContainsKey(vertex))
                _shortestPaths[vertex] = route;

            if (_graph.ContainsKey(vertex))
                foreach (var edge in _graph[vertex])
                    if (!_includedVertices[edge.VertexTo] && (_shortestPaths[edge.VertexTo] == null || route + edge.Weight < _shortestPaths[edge.VertexTo]))
                        _shortestPaths[edge.VertexTo] = route + edge.Weight;
        }

        private void SendMin(long vertexFrom, long vertexTo, double route)
        {
            _dijkstraProxy.Invoke("UpdateMin", vertexFrom, vertexTo, route);
        }

        private void SendSuccess()
        {
            _dijkstraProxy.Invoke("Result", true);
        }

        private void SendFailure()
        {
            _dijkstraProxy.Invoke("Result", false);
        }

        private void SaveResults()
        {
            if (_destination == null)
                _dijkstra.SaveResults(_shortestPaths);
            else
                _dijkstra.SaveResults(_shortestPaths[_destination.Value]);
        }

        private void Clean()
        {
            _dijkstra = null;
            _graph = null;
            _shortestPaths = null;
            _includedVertices = null;
        }
    }
}

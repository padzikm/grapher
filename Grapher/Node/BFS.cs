﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Client;
using Microsoft.AspNet.SignalR.Client;
using StackExchange.Redis;

namespace Node
{
    class BFS : IAlgorithm
    {
        private const string _bfsHubName = "BFSHub";
        private readonly TextWriter _logWriter;
        private readonly IDatabase _database;
        private IHubProxy _bfsProxy;
        private readonly AssemblyLoader<IBFS> _loader;
        private IBFS _bfs;
        private long _vertexFromOut;
        private long _vertexToOut;
        private long _vertexFromIn;
        private long _vertexToIn;
        private Dictionary<long, List<Edge>> _graph;
        private Dictionary<long, long?> _distances;
        private long _computedDistance;
        private long _root;
        private long? _destination;
        private bool _success;
        private string _abortMessage;
        private AutoResetEvent _event;
        private bool _abort;

        public BFS(ConnectionMultiplexer multiplexer, TextWriter logWriter)
        {
            _database = multiplexer.GetDatabase();
            _logWriter = logWriter;
            _loader = new AssemblyLoader<IBFS>();
        }

        public void RegisterProxy(HubConnection connection)
        {
            _bfsProxy = connection.CreateHubProxy(_bfsHubName);

            RegisterEvents();
        }

        public Task<Tuple<bool, string>> Compute(string workingDirectory, long vertexFromOut, long vertexToOut, long vertexFromIn, long vertexToIn, IEnumerable<string> files, bool restore)
        {
            _vertexFromOut = vertexFromOut;
            _vertexFromIn = vertexFromIn;
            _vertexToOut = vertexToOut;
            _vertexToIn = vertexToIn;
            _success = true;
            _abortMessage = null;
            _event = new AutoResetEvent(false);
            _abort = false;

            var task = new Task<Tuple<bool, string>>(() =>
            {
                _event.WaitOne();
                var res = new Tuple<bool, string>(_success, _abortMessage);
                Clean();
                return res;
            });

            task.Start();

            IEnumerable<Edge> tmpGraph = null;
            try
            {
                _bfs = _loader.Load(files, workingDirectory);
                if (_bfs == null)
                    throw new Exception("Required type not implemented");
                _root = _bfs.Root;
                _destination = _bfs.Destination;
                tmpGraph = _bfs.LoadGraph(_vertexFromOut, _vertexToOut, _vertexFromIn, _vertexToIn);
                tmpGraph = tmpGraph ?? new Edge[0];
            }
            catch (Exception ex)
            {
                _abortMessage = ex.Message;
                SendFailure();
                return task;
            }

            try
            {
                Initialize(tmpGraph);
                if (tmpGraph.LongCount() == 0 || (_destination != null && _root == _destination))
                {
                    SendSuccess();
                    return task;
                }
                tmpGraph = null;
                
                if (restore)
                    Restore();
                Compute();
            }
            catch (Exception ex)
            {
                _success = false;
                _abortMessage = ex.Message;
            }

            return task;
        }

        public void Abort()
        {
            _bfsProxy.Invoke("CleanAll");
            _abort = true;
            _event.Set();
        }

        private void RegisterEvents()
        {
            _bfsProxy.On("UpdateGraph", (long computedDistance) =>
            {
                try
                {
                    _computedDistance = computedDistance;
                    DownloadFront();
                    Compute();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                }
            });

            _bfsProxy.On("SaveResults", () =>
            {
                try
                {
                    SaveResults();
                    _success = true;
                    _event.Set();
                }
                catch (Exception ex)
                {
                    _success = false;
                    _abortMessage = ex.Message;
                    _event.Set();
                }
            });
        }

        private void Initialize(IEnumerable<Edge> edges)
        {
            _graph = new Dictionary<long, List<Edge>>();
            _distances = new Dictionary<long, long?>();
            _computedDistance = 0;

            foreach (var edge in edges)
            {
                if(edge == null)
                    continue;

                if (!_graph.ContainsKey(edge.VertexFrom))
                    _graph.Add(edge.VertexFrom, new List<Edge>());

                _graph[edge.VertexFrom].Add(edge);

                if (!_distances.ContainsKey(edge.VertexFrom))
                    _distances.Add(edge.VertexFrom, null);

                if (!_distances.ContainsKey(edge.VertexTo))
                    _distances.Add(edge.VertexTo, null);
            }

            if (_distances.ContainsKey(_root))
                _distances[_root] = 0;
        }

        private void Restore()
        {
            var keys = _distances.Select(p => (RedisValue)p.Key).ToArray();

            var transaction = _database.CreateTransaction();

            var entriesTask = transaction.HashGetAsync("BfsResults", keys);
            var distTask = transaction.StringGetAsync("BfsCurrentDistance");

            transaction.Execute();

            var entries = entriesTask.Result;
            var dist = distTask.Result;

            _computedDistance = dist.IsNull ? 0 : (long)dist;

            for (var i = 0L; i < entries.LongLength; ++i)
                if (!entries[i].IsNull && (long)entries[i] <= _computedDistance)
                    _distances[(long)keys[i]] = (long)entries[i];
        }

        private void Compute()
        {
            try
            {
                var front = UpdateFront();

                if (front != null)
                {
                    if (front.Count > 0)
                    {
                        UploadFront(front);

                        UpdateGraph(front);

                        if (_destination != null && front.Contains(_destination.Value))
                            Barrier(true, true);
                        else
                            Barrier(true, false);
                    }
                    else
                        Barrier(false, false);
                }
                else
                    SendSuccess();
            }
            catch (Exception ex)
            {
                _success = false;
                _abortMessage = ex.Message;
                SendFailure();
            }
        }

        private HashSet<long> UpdateFront()
        {
            var newFront = new HashSet<long>();
            var work = true;

            if (_distances.LongCount(p => p.Value != null) == _distances.Count)
                return null;

            foreach (var vertex in _graph)
                if (_distances[vertex.Key] == _computedDistance)
                {
                    foreach (var edge in vertex.Value)
                        if (_distances[edge.VertexTo] == null)
                        {
                            work = _bfs.PreVertexVisit(edge.VertexTo);
                            if (work)
                            {
                                newFront.Add(edge.VertexTo);
                                work = _bfs.PostVertexVisit(edge.VertexTo);
                            }

                            if (!work)
                                return newFront;
                        }
                }

            return newFront;
        }

        private void UpdateGraph(HashSet<long> front)
        {
            foreach (var vertex in front)
                _distances[vertex] = _computedDistance + 1;
        }

        private void UploadFront(HashSet<long> front)
        {
            var dist = _computedDistance + 1;
            var entries = front.Select(p => new HashEntry(p, dist)).ToArray();

            _database.HashSet("BfsResults", entries);
        }

        private void DownloadFront()
        {
            var keys = _distances.Select(p => (RedisValue)p.Key).ToArray();

            var entries = _database.HashGet("BfsResults", keys);

            for (var i = 0L; i < entries.LongLength; ++i)
                if (!entries[i].IsNull && _distances[(long)keys[i]] == null)
                    _distances[(long)keys[i]] = (long)entries[i];
        }

        private void Barrier(bool found, bool destinationFound)
        {
            _bfsProxy.Invoke("Barrier", found, destinationFound);
        }

        private void SendSuccess()
        {
            _bfsProxy.Invoke("Result", true);
        }

        private void SendFailure()
        {
            _bfsProxy.Invoke("Result", false);
        }

        private void SaveResults()
        {
            if (_destination == null)
                _bfs.SaveResults(_distances);
            else
                _bfs.SaveResults(_distances[_destination.Value]);
        }

        private void Clean()
        {
            _bfs = null;
            _graph = null;
            _distances = null;
        }
    }
}

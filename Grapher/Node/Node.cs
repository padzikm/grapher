﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Microsoft.AspNet.SignalR.Client;
using StackExchange.Redis;

namespace Node
{
    class Node
    {
        private readonly ConnectionMultiplexer _multiplexer;
        private readonly string _currentDirectory;
        private readonly TextWriter _logWriter;
        private readonly ZipExtractor _extractor;
        private readonly HubConnection _hubConnection;
        private readonly IHubProxy _graphProxy;
        private readonly Dictionary<string, IAlgorithm> _algorithms;
        private IAlgorithm _currentAlgorithm;
        private bool _isAborted;

        public Node(string currentDirectory, TextWriter logWriter)
        {
            _currentDirectory = currentDirectory;
            _logWriter = logWriter;
            _extractor = new ZipExtractor();
            var redis = ConfigurationManager.ConnectionStrings["Redis"].ConnectionString;
            _multiplexer = ConnectionMultiplexer.Connect(redis);
            var server = ConfigurationManager.ConnectionStrings["Server"].ConnectionString;
            _hubConnection = new HubConnection(server);
            _graphProxy = _hubConnection.CreateHubProxy("GraphHub");
            _algorithms = new Dictionary<string, IAlgorithm>()
            {
                { "dijkstra", new Dijkstra(_multiplexer, logWriter) },
                { "minimumspanningtree", new MinimumSpanningTree(_multiplexer, logWriter) },
                { "bfs", new BFS(_multiplexer, logWriter) },
            };
        }

        public string Start()
        {
            try
            {
                RegisterEvents();

                foreach (var algorithm in _algorithms.Values)
                    algorithm.RegisterProxy(_hubConnection);

                _hubConnection.Start().Wait();

                _logWriter.WriteLine("\nConnection type: " + _hubConnection.Transport.Name);
                
                var status = _graphProxy.Invoke<bool>("Register").Result;

                return status ? _hubConnection.ConnectionId : null;
            }
            catch (Exception ex)
            {
                _logWriter.WriteLine("\n" + ex.Message + "\n");
                return null;
            }
        }

        public void Stop()
        {
            _hubConnection.Stop();
        }

        private void RegisterEvents()
        {
            _graphProxy.On("Compute", (string algorithmName, long vertexOutFrom, long vertexOutTo, long vertexInFrom, long vertexInTo, byte[] data, bool restore) =>
            {
                _logWriter.WriteLine("Server send:\nAlgorithm: {0}\nVertexFromOut: {1}\nVertexToOut: {2}\nVertexFromIn: {3}\nVertexToIn: {4}\nBytes: {5}\nRestore: {6}\n",
                    algorithmName, vertexOutFrom, vertexOutTo, vertexInFrom, vertexInTo, data.LongLength, restore);

                Compute(algorithmName, vertexOutFrom, vertexOutTo, vertexInFrom, vertexInTo, data, restore);
            });

            _graphProxy.On("Abort", () =>
            {
                _isAborted = true;
                _currentAlgorithm.Abort();
                _logWriter.WriteLine("Computations aborted\n");
            });
        }

        private async void Compute(string algorithmName, long vertexOutFrom, long vertexOutTo, long vertexInFrom, long vertexInTo, byte[] data, bool restore)
        {
            var files = null as IEnumerable<string>;

            var dirName = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '_');
            var dirPath = @_currentDirectory + @"\" + dirName + @"\";
            Directory.CreateDirectory(dirPath);

            _logWriter.WriteLine("Working dir: \n" + dirPath);

            try
            {
                files = _extractor.Extract(dirPath, data);
            }
            catch (Exception ex)
            {
                _graphProxy.Invoke("Result", false, "Archive error");
                _logWriter.WriteLine("Archive error\n");
                return;
            }

            var algName = algorithmName.ToLower();

            if (_algorithms.TryGetValue(algName, out _currentAlgorithm))
            {
                _isAborted = false;

                var task = _currentAlgorithm.Compute(dirPath, vertexOutFrom, vertexOutTo, vertexInFrom, vertexInTo, files, restore);

                var res = await task;

                var success = res.Item1;
                var msg = res.Item2 ?? "";

                _logWriter.WriteLine("Computations successful: {0}\n", success);

                if (!_isAborted)
                    _graphProxy.Invoke("Result", success, msg);
                else
                    _graphProxy.Invoke("Result", false, msg);
            }
            else
            {
                _graphProxy.Invoke("Result", false, "No matching algorithm");
                _logWriter.WriteLine("No matching algorithm\n");
            }
        }
    }
}

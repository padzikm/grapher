﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Client;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace TestDll
{
    public class GraphAlgorithm : IGraph
    {
        private const string FileName = @"TestDll.dll.config";

        protected GraphClient GraphClient { get; private set; }

        public string Directory { get; set; }

        public IEnumerable<Edge> LoadGraph(long fromOut, long toOut, long fromIn, long toIn)
        {
            Connect();

            var query = GraphClient.Cypher.Match("(n)-[e]->(m)")
                .Where("n.Id >= " + fromOut)
                .AndWhere("n.Id <= " + toOut)
                .AndWhere("m.Id >= " + fromIn)
                .AndWhere("m.Id <= " + toIn)
                .Return((n, e, m) => new Tuple<long, long, double>(Return.As<long>("n.Id"), Return.As<long>("m.Id"), Return.As<double>("e.weight")));

            var results = query.Results;
            var edges = results.Select(p => new Edge(p.Item1, p.Item2, p.Item3));

            return edges;
        }

        private void Connect()
        {
            var uri = Directory + FileName;

            using (var tr = new StreamReader(uri))
            {
                var reader = XDocument.Load(tr);

                var conf = reader.Element("configuration");
                var connStrings = conf.Element("connectionStrings");
                var neo4jEl = connStrings.Elements().Single(p => p.Attribute("name").Value == "Neo4j");
                var neo4jAttr = neo4jEl.Attribute("connectionString");
                var neo4j = neo4jAttr.Value;

                GraphClient = new GraphClient(new Uri(neo4j));

                GraphClient.Connect();
            }
        }
    }
}

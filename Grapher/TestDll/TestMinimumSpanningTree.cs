﻿using System.Collections.Generic;
using Client;

namespace TestDll
{
    public class TestMinimumSpanningTree : GraphAlgorithm, IMinimumSpanningTree
    {
        public long Root { get { return 0; } }

        public void SaveResults(IEnumerable<Edge> includedEdges)
        {
            var query = GraphClient.Cypher.Unwind(includedEdges, "edges")
                    .Match("(n)-[e]->(m)")
                    .Where("n.Id = edges.VertexFrom")
                    .AndWhere("m.Id = edges.VertexTo")
                    .AndWhere("e.weight = edges.Weight")
                    .Set("e.mstGrapher = 1");

            query.ExecuteWithoutResults();
        }
    }
}

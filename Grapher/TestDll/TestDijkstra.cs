﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client;
using Neo4jClient;

namespace TestDll
{
    public class TestDijkstra : GraphAlgorithm, IDijkstra
    {
        public long Root { get { return 0; } }

        public long? Destination { get { return null; } }

        public void SaveResults(Dictionary<long, double?> result)
        {
            var list = result.Select(p => new { Id = p.Key, Route = p.Value });

            var query = GraphClient.Cypher.Unwind(list, "route")
                .Match("(n)")
                .Where("n.Id = route.Id")
                .Set("n.dijkstraGrapher = route.Route");

            var succeded = false;

            while (!succeded)
            {
                try
                {
                    query.ExecuteWithoutResults();
                    succeded = true;
                }
                catch (NeoException ex)
                {
                    succeded = false;
                }
            }
        }

        public void SaveResults(double? result)
        {
            var query = GraphClient.Cypher.Match("(n)")
                .Where("n.Id = " + Destination.Value)
                .Set("e.dijkstraGrapher = " + result);

            var succeded = false;

            while (!succeded)
            {
                try
                {
                    query.ExecuteWithoutResults();
                    succeded = true;
                }
                catch (NeoException ex)
                {
                    succeded = false;
                }
            }
        }
    }
}

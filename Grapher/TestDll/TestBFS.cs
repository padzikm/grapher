﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client;
using Neo4jClient;

namespace TestDll
{
    public class TestBFS : GraphAlgorithm, IBFS
    {
        public long Root { get { return 86; } }

        public long? Destination { get { return null; } }

        public bool PreVertexVisit(long vertexId)
        {
            return true;
        }

        public bool PostVertexVisit(long vertexId)
        {
            return true;
        }

        public void SaveResults(Dictionary<long, long?> results)
        {
            var list = results.Select(p => new { Id = p.Key, Route = p.Value });

            var query = GraphClient.Cypher.Unwind(list, "route")
                .Match("(n)")
                .Where("n.Id = route.Id")
                .Set("n.bfsGrapher = route.Route");

            var succeded = false;

            while (!succeded)
            {
                try
                {
                    query.ExecuteWithoutResults();
                    succeded = true;
                }
                catch (NeoException ex)
                {
                    succeded = false;
                }
            }
        }

        public void SaveResults(long? result)
        {
            var query = GraphClient.Cypher.Match("(n)")
                .Where("n.Id = " + Destination.Value)
                .Set("e.bfsGrapher = " + result);

            var succeded = false;

            while (!succeded)
            {
                try
                {
                    query.ExecuteWithoutResults();
                    succeded = true;
                }
                catch (NeoException ex)
                {
                    succeded = false;
                }
            }
        }
    }
}

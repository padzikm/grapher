﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json;

namespace DataGenerator
{
    class Neo4jGraphService
    {
        private readonly GraphClient _client;

        public Neo4jGraphService()
        {
            _client = new GraphClient(new Uri("http://localhost:7474/db/data"), new HttpClientWrapper(new HttpClient() { Timeout = new TimeSpan(0, 5, 0) }));
            _client.Connect();
        }

        public void Load(string path)
        {
            using (var file = File.OpenText(path))
            {
                var lineNo = 0L;

                while (!file.EndOfStream)
                {
                    var tmp = file.ReadLine();
                    tmp = tmp.TrimEnd(',');

                    var line = tmp.Split(',');

                    var sb = new StringBuilder();
                    var second = new StringBuilder();

                    for (long i = 0; i < line.LongLength; ++i)
                        if (line[i] != "0")
                        {
                            if (i < line.LongLength / 2)
                                sb.Append(string.Format(",'{0}.{1}'", line[i], i));
                            else
                                second.Append(string.Format(",'{0}.{1}'", line[i], i));
                        }

                    if (sb.Length == 0 && second.Length == 0)
                    {
                        var query = _client.Cypher.Merge(string.Format("(n {{Id:{0}}})", lineNo));

                        query.ExecuteWithoutResults();
                    }

                    if (sb.Length > 0)
                    {
                        sb = sb.Remove(0, 1);

                        var query = _client.Cypher.Merge(string.Format("(n {{Id:{0}}})", lineNo))
                            .With(string.Format("n, [{0}] as coll", sb))
                            .ForEach(
                                "(x in coll | merge(m {Id:toInt(split(x,'.')[1])}) create(n-[:Edge {weight:toInt(split(x,'.')[0])}]->m))");

                        query.ExecuteWithoutResults();
                    }

                    if (second.Length > 0)
                    {
                        second = second.Remove(0, 1);

                        var query = _client.Cypher.Merge(string.Format("(n {{Id:{0}}})", lineNo))
                            .With(string.Format("n, [{0}] as coll", second))
                            .ForEach(
                                "(x in coll | merge(m {Id:toInt(split(x,'.')[1])}) create(n-[:Edge {weight:toInt(split(x,'.')[0])}]->m))");

                        query.ExecuteWithoutResults();
                    }

                    ++lineNo;
                }
            }
        }

        public void MinimalSpannigTree(long rootId = 0)
        {
            _client.Cypher.Match("(node)").Where("node.Id = " + rootId).Set("node : MST").ExecuteWithoutResults();
            _client.Cypher.Match("(node)").Where("node.Id <> " + rootId).Set("node : Free").ExecuteWithoutResults();

            var query = _client.Cypher.Match("(from:MST)-[e:Edge]->(to:Free)")
                .With("e, to")
                .OrderBy("e.weight")
                .Limit(1)
                .Remove("to:Free")
                .Set("to:MST")
                .Set("e.mst=1")
                .Return<long>("to.Id");

            var result = query.Results.LongCount();

            while (result > 0)
                result = query.Results.LongCount();
        }

        public void Dijkstra(long rootId)
        {
            var verticesCount = _client.Cypher.Match("(n)").Return(n => Return.As<long>("count(n)")).Results.First();

            for (long i = 0; i < verticesCount; ++i)
                Dijkstra(rootId, i);
        }

        public long? Dijkstra(long rootId, long destId)
        {
            var ids = _client.Cypher.Match("(n {Id:" + rootId + "}), (m {Id:" + destId + "})")
                    .Return((n, m) => new Tuple<long, long>(Return.As<long>("id(n)"), Return.As<long>("id(m)"))).Results.First();

            using (var wb = new WebClient())
            {
                var toUri = "http://localhost:7474/db/data/node/" + ids.Item2;

                var obj = new
                {
                    to = toUri,
                    cost_property = "weight",
                    relationships = new { type = "Edge", direction = "out" },
                    algorithm = "dijkstra"
                };

                var json = JsonConvert.SerializeObject(obj);

                var uri = "http://localhost:7474/db/data/node/" + ids.Item1 + "/path";

                long? res = null;

                try
                {
                    var response = wb.UploadString(uri, json);

                    dynamic parsed = JsonConvert.DeserializeObject(response);

                    var tmp = parsed["weight"].ToString();

                    res = long.Parse(tmp);

                    _client.Cypher.Match("(n {Id: {params}})").WithParam("params", destId).Set("n.dijkstra={param}").WithParam("param", res).ExecuteWithoutResults();
                }
                catch
                {
                }

                return res;
            }
        }

        public void Bfs(long rootId, long? dest = null)
        {
            var query = _client.Cypher.Match("(root {Id:{rootId}})-->(n)")
                    .WithParam("rootId", rootId)
                    .Set("root.bfs = 0")
                    .Set("n.bfs = 1")
                    .Return((n) => Return.As<long>("n.Id"));

            var neighbours = query.Results.ToList();

            if (dest != null && (rootId == dest || neighbours.Contains(dest.Value)))
                return;

            var distance = 2L;

            while (neighbours.Count > 0)
            {
                query = _client.Cypher.Match("(n)-->(m)")
                    .Where("m.bfs is null")
                    .AndWhere("n.Id in {list}")
                    .WithParam("list", neighbours)
                    .Set("m.bfs = {dist}")
                    .WithParam("dist", distance)
                    .ReturnDistinct((m) => Return.As<long>("m.Id"));

                neighbours = query.Results.ToList();

                if (dest != null && neighbours.Contains(dest.Value))
                    return;

                ++distance;
            }
        }

        public bool BfsPathExists(long rootId, long destId)
        {
            var query = _client.Cypher.Match("path=shortestPath((root {Id:{rootParams}})-[*]->(dest {Id:{destParams}}))")
                .WithParam("rootParams", new { Id = rootId })
                .WithParam("destParams", new { Id = destId })
                .Return((path) => Return.As<long>("length(path)"));

            return query.Results.First() > 0;
        }

        public long BfsShortestPath(long rootId, long destId)
        {
            var query = _client.Cypher.Match("path=shortestPath((root {Id:{rootParams}})-[*]->(dest {Id:{destParams}}))")
                .WithParam("rootParams", new { Id = rootId })
                .WithParam("destParams", new { Id = destId })
                .Return((path) => Return.As<long>("length(path)"));

            var res = query.Results.First();

            return res > 0 ? res : -1;
        }

        private bool CompareMstResults(string path)
        {
            var result = GetMstResults();
            var pairs = new List<Tuple<long, long>>();

            using (var reader = File.OpenText(path))
            {
                var content = reader.ReadToEnd();
                var fact = content.Split('\n').ToList();
                fact.RemoveAt(fact.Count - 1);

                pairs = fact.Select(line =>
                {
                    var tmp = line.Split('\t');
                    return new Tuple<long, long>(long.Parse(tmp[0]), long.Parse(tmp[1]));
                }).ToList();
            }

            if (pairs.Count != result.Count())
                return false;

            foreach (var pair in pairs)
                if (result.SingleOrDefault(p => (p.Item1 == pair.Item1 && p.Item2 == pair.Item2) || (p.Item2 == pair.Item1 && p.Item1 == pair.Item2)) == null)
                    return false;

            return true;
        }

        private List<Tuple<long, long>> GetMstResults()
        {
            var query = _client.Cypher.Match("(from:MST)-[e:Edge]->(to:MST)")
                .Where("has(e.mst)")
                .AndWhere("e.mst = 1")
                .Return((from, to) => new Tuple<long, long>(Return.As<long>("from.Id"), Return.As<long>("to.Id")));

            return query.Results.ToList();
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace DataGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var argList = args.Select(p => p.ToLowerInvariant()).ToList();

            if (argList.Contains("generate"))
            {
                Console.WriteLine("\nType number of vertices");

                var str1 = Console.ReadLine();
                int verticesCount;
                double density;

                Console.WriteLine("Type density of the graph");

                var str2 = Console.ReadLine();

                if (!int.TryParse(str1, out verticesCount) || !double.TryParse(str2, NumberStyles.Float, CultureInfo.InvariantCulture, out density))
                    throw new ArgumentException("Arguments must be numbers");
                else
                {
                    Console.WriteLine("Type graph type (mst | dijkstra | bfs)");

                    var str3 = Console.ReadLine();

                    if (str3.ToLower() == "mst")
                    {
                        var g = new MinimalSpanningTreeGenerator();
                        g.GenerateGraph(verticesCount, density);
                    }
                    else if (str3.ToLower() == "dijkstra")
                    {
                        int start, end, pathLength;

                        Console.WriteLine("Type start vertex");

                        var s1 = Console.ReadLine();

                        if(!int.TryParse(s1, out start))
                            throw new ArgumentException("Argument must be number");

                        Console.WriteLine("Type end vertex");

                        var s2 = Console.ReadLine();

                        if(!int.TryParse(s2, out end))
                            throw new ArgumentException("Argument must be number");

                        Console.WriteLine("Type path length");

                        var s3 = Console.ReadLine();

                        if(!int.TryParse(s3, out pathLength))
                            throw new ArgumentException("Argument must be number");

                        var g = new DijkstraGenerator();
                        
                        g.GenerateGraph(verticesCount, density, start, end, pathLength);
                    }
                    else if (str3.ToLower() == "bfs")
                    {
                        int pathLength;

                        Console.WriteLine("Type path length");

                        var s3 = Console.ReadLine();

                        if (!int.TryParse(s3, out pathLength))
                            throw new ArgumentException("Argument must be number");

                        var g = new BFSGenerator();

                        g.GenerateGraph(verticesCount, density, pathLength);
                    }
                    else
                    {
                        Console.WriteLine("\nNot recognized algorithm\n");
                        return;
                    }

                    Console.WriteLine("\nGraph has been generated\n");

                    if (argList.Count == 1)
                        return;
                }
            }

            var service = new Neo4jGraphService();
            
            if (argList.Contains("load"))
            {
                service.Load("graph.csv");
                Console.WriteLine("Graph has been loaded\n");
            }

            if (argList.Contains("mst"))
                Execute("Mst", () => service.MinimalSpannigTree());

            if (argList.Contains("dijkstraall"))
            {
                Console.WriteLine("Type rootId:");
                var arg = Console.ReadLine();
                long rootId;

                if(!long.TryParse(arg, out rootId))
                    throw new ArgumentException("Argument must be number");
                
                Execute("Dijkstra all", () => service.Dijkstra(rootId));
            }

            if (argList.Contains("dijkstraone"))
            {
                Console.WriteLine("Type rootId:");
                var arg1 = Console.ReadLine();
                Console.WriteLine("Type destId:");
                var arg2 = Console.ReadLine();
                long rootId, destId;

                if (!long.TryParse(arg1, out rootId) || !long.TryParse(arg2, out destId))
                    throw new ArgumentException("Arguments must be numbers");

                Execute("Dijkstra one", () => service.Dijkstra(rootId, destId));
            }

            if (argList.Contains("bfsall"))
            {
                Console.WriteLine("Type rootId:");
                var arg = Console.ReadLine();
                long rootId;

                if (!long.TryParse(arg, out rootId))
                    throw new ArgumentException("Argument must be number");

                Execute("BFS all", () => service.Bfs(rootId));
            }

            if (argList.Contains("bfsone"))
            {
                Console.WriteLine("Type rootId:");
                var arg1 = Console.ReadLine();
                Console.WriteLine("Type destId:");
                var arg2 = Console.ReadLine();
                long rootId, destId;

                if (!long.TryParse(arg1, out rootId) || !long.TryParse(arg2, out destId))
                    throw new ArgumentException("Arguments must be numbers");

                Execute("BFS one", () => service.Bfs(rootId, destId));
            }
        }

        private static void Execute(string algorithmName, Action action)
        {
            var watch = new Stopwatch();

            watch.Start();

            action();

            watch.Stop();

            Console.WriteLine("{0} time: {1}h {2}min {3}s {4}ms\n", algorithmName, watch.Elapsed.Hours, watch.Elapsed.Minutes, watch.Elapsed.Seconds, watch.Elapsed.Milliseconds);
        }
    }
}

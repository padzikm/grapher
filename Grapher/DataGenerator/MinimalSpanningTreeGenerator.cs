﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataGenerator
{
    public class MinimalSpanningTreeGenerator
    {
        public void GenerateGraph(long verticesCount, double density)
        {
            var r = new Random();

            var minTree = new long[verticesCount - 1, 2];

            //density -= (double)(verticesCount - 1) / (double)(verticesCount * verticesCount);

            var used = new List<long>();
            var unused = new List<long>();

            for (int i = 0; i < verticesCount; ++i)
            {
                unused.Add(i);
            }

            // first vertx
            long tmp = unused[r.Next(0, unused.Count)];
            minTree[0, 0] = tmp;
            used.Add(tmp);
            unused.Remove(tmp);

            // second vertx
            tmp = unused[r.Next(0, unused.Count)];
            minTree[0, 1] = tmp;
            used.Add(tmp);
            unused.Remove(tmp);

            for (int i = 1; i < verticesCount - 1; ++i)
            {
                tmp = used[r.Next(0, used.Count)];
                minTree[i, 0] = tmp;

                tmp = unused[r.Next(0, unused.Count)];
                minTree[i, 1] = tmp;
                used.Add(tmp);
                unused.Remove(tmp);

            }

            using (StreamWriter sw = new StreamWriter("mstResults.txt"))
            {
                for (int i = 0; i < verticesCount - 1; ++i)
                {
                    sw.WriteLine("{0}\t{1}", minTree[i, 0], minTree[i, 1]);
                }
            }

            // create .csv file
            using (StreamWriter sw = new StreamWriter("tmp_graph.csv"))
            {



                for (int i = 0; i < verticesCount; ++i)
                {

                    var vertices = new List<long>();
                    for (int j = 0; j < minTree.GetLength(0); ++j)
                    {
                        if (minTree[j, 0] == i)
                            vertices.Add(minTree[j, 1]);

                        if (minTree[j, 1] == i)
                            vertices.Add(minTree[j, 0]);
                    }

                    StringBuilder s = new StringBuilder();
                    for (int j = 0; j <= i; ++j)
                    {
                        s.Append("0,");
                    }

                    for (int j = i + 1; j < verticesCount; ++j)
                    {

                        if (vertices.Contains(j))
                            s.Append("1,");
                        else
                        {

                            if (r.NextDouble() < density)
                                s.Append(r.Next(2, 9).ToString() + ",");
                            else
                                s.Append("0,");
                        }
                    }
                    s.Remove(s.Length - 1, 1);
                    sw.WriteLine(s.ToString());
                }


            }

            using (StreamWriter sw = new StreamWriter("graph.csv"))
            {
                using (StreamReader sr = new StreamReader("tmp_graph.csv"))
                {
                    for (int i = 0; i < verticesCount; ++i)
                    {
                        StringBuilder s = new StringBuilder();

                        string row = sr.ReadLine();
                        string[] arr = row.Split(',');

                        using (StreamReader sr1 = new StreamReader("tmp_graph.csv"))
                        {
                            for (int j = 0; j < i; ++j)
                            {
                                string tmp_row = sr1.ReadLine();
                                string[] tmp_arr = tmp_row.Split(',');
                                string tmp2 = tmp_arr[i];
                                s.Append(",");
                                s.Append(tmp2);
                            }

                            for (int j = i; j < verticesCount; ++j)
                            {

                                string tmp2 = arr[j];
                                s.Append(",");
                                s.Append(tmp2);
                            }

                            var st = s.Remove(0, 1);

                            sw.WriteLine(st.ToString());
                        }
                    }
                }
            }

            File.Delete("tmp_graph.csv");
        }
    }
}

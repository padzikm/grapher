﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataGenerator
{
    class BFSGenerator
    {
        public void GenerateGraph(int verticesCount, double density, int edgesCount)
        {
            if (File.Exists("graph_metadata.txt"))
                File.Delete("graph_metadata.txt");

            if (File.Exists("graph.csv"))
                File.Delete("graph.csv");

            if (File.Exists("tmp_graph.csv"))
                File.Delete("tmp_graph.csv");

            // create path

            Random r = new Random();

            int[,] Path = new int[edgesCount, 2];

            List<int> used = new List<int>();
            List<int> unused = new List<int>();

            int verticesInPath = edgesCount - 1;

            for (int i = verticesCount - verticesInPath; i < verticesCount; ++i)
            {
                unused.Add(i);
            }

            int inVertex = r.Next(0, (verticesCount - verticesInPath) / 2);
            int outVertex = r.Next((verticesCount - verticesInPath) / 2, verticesCount - verticesInPath);

            //first vertex
            Path[0, 0] = inVertex;

            // second vertx
            int last;
            int tmp;
            tmp = unused[r.Next(0, unused.Count)];
            Path[0, 1] = tmp;
            used.Add(tmp);
            unused.Remove(tmp);

            last = tmp;

            for (int i = 1; i < edgesCount - 1; ++i)
            {
                Path[i, 0] = last;

                tmp = unused[r.Next(0, unused.Count)];
                Path[i, 1] = tmp;
                used.Add(tmp);
                unused.Remove(tmp);
                last = tmp;
            }

            Path[edgesCount - 1, 0] = last;
            Path[edgesCount - 1, 1] = outVertex;

            using (StreamWriter sw = new StreamWriter("graph_metadata.txt"))
            {
                string vc = "vertices count: " + verticesCount.ToString();
                sw.WriteLine(vc);
                string d = "density: " + density.ToString();
                sw.WriteLine(d);

                sw.WriteLine("shortest path between vertex {0} and {1}:", inVertex, outVertex);
                for (int i = 0; i < edgesCount; ++i)
                {
                    sw.WriteLine(Path[i, 0].ToString() + "\t" + Path[i, 1].ToString());
                }
            }

            // create .csv file
            using (StreamWriter sw = new StreamWriter("tmp_graph.csv"))
            {

                for (int i = 0; i < (verticesCount - verticesInPath) / 2; ++i)
                {
                    StringBuilder s = new StringBuilder();
                    for (int j = 0; j <= i; ++j)
                    {
                        s.Append("0,");
                    }

                    for (int j = i + 1; j < (verticesCount - verticesInPath) / 2; ++j)
                    {
                        if (r.NextDouble() < density)
                            s.Append("1,");
                        else
                            s.Append("0,");
                    }

                    for (int j = (verticesCount - verticesInPath) / 2; j < verticesCount; ++j)
                    {
                        if (j == Path[0, 1] && i == Path[0, 0])
                            s.Append("1,");
                        else
                            s.Append("0,");
                    }

                    s.Remove(s.Length - 1, 1);
                    sw.WriteLine(s.ToString());
                }

                for (int i = (verticesCount - verticesInPath) / 2; i < verticesCount - verticesInPath; ++i)
                {
                    StringBuilder s = new StringBuilder();
                    for (int j = 0; j <= i; ++j)
                    {
                        s.Append("0,");
                    }

                    for (int j = i + 1; j < verticesCount - verticesInPath; ++j)
                    {
                        if (r.NextDouble() < density)
                            s.Append("1,");
                        else
                            s.Append("0,");
                    }

                    for (int j = verticesCount - verticesInPath; j < verticesCount; ++j)
                    {

                        if (j == Path[edgesCount - 1, 0] && i == Path[edgesCount - 1, 1])
                            s.Append("1,");
                        else
                            s.Append("0,");
                    }
                    s.Remove(s.Length - 1, 1);
                    sw.WriteLine(s.ToString());
                }

                for (int i = verticesCount - verticesInPath; i < verticesCount; ++i)
                {
                    List<int> vertices = new List<int>();
                    for (int j = 0; j < Path.GetLength(0); ++j)
                    {
                        if (Path[j, 0] == i)
                            vertices.Add(Path[j, 1]);

                        if (Path[j, 1] == i)
                            vertices.Add(Path[j, 0]);
                    }

                    StringBuilder s = new StringBuilder();
                    for (int j = 0; j <= i; ++j)
                    {
                        s.Append("0,");
                    }

                    for (int j = i + 1; j < verticesCount; ++j)
                    {
                        if (vertices.Contains(j))
                            s.Append("1,");
                        else
                            s.Append("0,");
                    }

                    s.Remove(s.Length - 1, 1);
                    sw.WriteLine(s.ToString());
                }
            }

            // transpose matrix

            using (StreamWriter sw = new StreamWriter("graph.csv"))
            {

                //using (StreamReader sr = new StreamReader("tmp_graph.csv"))
                //{
                for (int i = 0; i < verticesCount; ++i)
                {
                    StringBuilder s = new StringBuilder();

                    //string row = sr.ReadLine();
                    //string[] arr = row.Split(',');

                    using (StreamReader sr = new StreamReader("tmp_graph.csv"))
                    {
                        for (int j = 0; j < i; ++j)
                        {
                            string tmp_row = sr.ReadLine();
                            string[] tmp_arr = tmp_row.Split(',');
                            string temp = tmp_arr[i];
                            s.Append(temp);
                            s.Append(",");
                        }

                        string row = sr.ReadLine();
                        string[] arr = row.Split(',');

                        for (int j = i; j < verticesCount; ++j)
                        {
                            string temp = arr[j];
                            s.Append(temp);
                            s.Append(",");
                        }

                        sw.WriteLine(s.ToString());
                    }
                }
            }
            File.Delete("tmp_graph.csv");
        }
    }
}

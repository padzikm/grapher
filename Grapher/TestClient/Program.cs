﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Client;

namespace TestClient
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            var path2 = Path.GetDirectoryName(path);
            var path3 = @path2 + @"\TestDll.dll";
            var path4 = @path2 + @"\Neo4jClient.dll";
            var path5 = @path2 + @"\TestDll.dll.config";
            var path6 = @path2 + @"\TestDll.pdb";
            var path7 = @path2 + @"\Newtonsoft.Json.dll";
            var path8 = @path2 + @"\System.Net.Http.Extensions.dll";
            var path9 = @path2 + @"\System.Net.Http.Primitives.dll";
            var path10 = @path2 + @"\Newtonsoft.Json.xml";
            var path11 = @path2 + @"\Neo4jClient.xml";
            var path12 = @path2 + @"\System.Net.Http.Primitives.xml";
            var path13 = @path2 + @"\System.Net.Http.Extensions.xml";
            var paths = new string[] { path3, path4, path5, path6, path7};//, path8, path9, path10, path11, path12, path13 };

            var grapher = new Grapher();

            Tuple<bool, IEnumerable<string>> result;
            Console.WriteLine("\nType algorithm name: (mst | dijkstra | bfs)\n");
            var algName = Console.ReadLine().ToLower();
            Console.WriteLine("\nType maximum vertex count:\n");
            var arg = Console.ReadLine();
            Console.WriteLine();
            var vertexCount = 0L;

            if (!long.TryParse(arg, out vertexCount))
                throw new ArgumentException("Argument must be number");

            var watch = new Stopwatch();
            watch.Start();

            if (algName == "mst")
                result = grapher.MinimumSpanningTree(vertexCount, paths).Result;
            else if (algName == "dijkstra")
                result = grapher.Dijkstra(vertexCount, paths).Result;
            else if (algName == "bfs")
                result = grapher.BFS(vertexCount, paths).Result;
            else
            {
                Console.WriteLine("\nNot recognized algorithm\n");
                return;
            }

            watch.Stop();

            Console.WriteLine("\nComputations succeded: {0}\n", result.Item1);
            Console.WriteLine("Messages list: \n");
            
            foreach (var msg in result.Item2)
                Console.WriteLine("{0}\n", msg);

            Console.WriteLine("\n{0} time: {1}h {2}min {3}s {4}ms\n", algName, watch.Elapsed.Hours, watch.Elapsed.Minutes, watch.Elapsed.Seconds, watch.Elapsed.Milliseconds);

            Console.ReadLine();
        }
    }
}

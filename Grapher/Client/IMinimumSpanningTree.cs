﻿using System.Collections.Generic;

namespace Client
{
    public interface IMinimumSpanningTree : IGraph
    {
        long Root { get; }

        void SaveResults(IEnumerable<Edge> includedEdges);
    }
}

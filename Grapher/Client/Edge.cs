﻿namespace Client
{
    public class Edge
    {
        public long VertexFrom { get; private set; }

        public long VertexTo { get; private set; }

        public double Weight { get; private set; }

        public Edge(long from, long to, double weight)
        {
            VertexFrom = from;
            VertexTo = to;
            Weight = weight;
        }
    }
}

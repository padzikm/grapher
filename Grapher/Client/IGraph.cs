﻿using System.Collections.Generic;

namespace Client
{
    public interface IGraph
    {
        string Directory { set; }

        IEnumerable<Edge> LoadGraph(long fromOut, long toOut, long fromIn, long toIn);
    }
}

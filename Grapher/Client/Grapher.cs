﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using SevenZip;

namespace Client
{
    public class Grapher
    {
        private readonly string _serverUri;
        private const string _hubName = "GraphHub";
        private const string _mstAlgorithm = "MinimumSpanningTree";
        private const string _dijkstraAlgorithm = "Dijkstra";
        private const string _bfsAlgorithm = "Bfs";
        private const int _dataChunk = 30000;

        public Grapher()
        {
            _serverUri = ConfigurationManager.ConnectionStrings["Grapher"].ConnectionString;
        }

        public Grapher(string uri)
        {
            _serverUri = uri;
        }

        public async Task<Tuple<bool, IEnumerable<string>>> MinimumSpanningTree(long verticesCount, params string[] paths)
        {
            var requiredTypes = new Type[] { typeof(IMinimumSpanningTree) };

            if (!Verify(paths, requiredTypes))
                throw new ArgumentException("Required types not implemented");

            return await Compute(_mstAlgorithm, verticesCount, paths);
        }

        public async Task<Tuple<bool, IEnumerable<string>>> Dijkstra(long verticesCount, params string[] paths)
        {
            var requiredTypes = new Type[] { typeof(IDijkstra) };

            if (!Verify(paths, requiredTypes))
                throw new ArgumentException("Required types not implemented");

            return await Compute(_dijkstraAlgorithm, verticesCount, paths);
        }

        public async Task<Tuple<bool, IEnumerable<string>>> BFS(long verticesCount, params string[] paths)
        {
            var requiredTypes = new Type[] { typeof(IBFS) };

            if (!Verify(paths, requiredTypes))
                throw new ArgumentException("Required types not implemented");

            return await Compute(_bfsAlgorithm, verticesCount, paths);
        }

        private bool Verify(string[] paths, Type[] requiredTypes)
        {
            var contained = new bool[requiredTypes.Length];

            var dlls = paths.Where(p => p.EndsWith(".dll")).ToList();

            foreach (var dll in dlls)
            {
                var assembly = Assembly.LoadFile(dll);
                var types = assembly.GetExportedTypes();

                foreach (var type in types)
                    for (var i = 0; i < requiredTypes.Length; ++i)
                        if (requiredTypes[i].IsAssignableFrom(type))
                            contained[i] = true;
            }

            return contained.All(p => p);
        }

        private async Task<Tuple<bool, IEnumerable<string>>> Compute(string algorithmName, long verticesCount, string[] paths)
        {
            var archiveName = Compress(paths);
            var data = Load(archiveName);

            var success = await Communicate(algorithmName, verticesCount, data);

            File.Delete(archiveName);

            return success;
        }

        private string Compress(string[] paths)
        {
            var dateTimeToken = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '_');
            var archiveName = @dateTimeToken + ".7z";
            var compressor = new SevenZipCompressor() { ArchiveFormat = OutArchiveFormat.SevenZip };
            compressor.CompressFiles(archiveName, paths);
            return archiveName;
        }

        private byte[] Load(string path)
        {
            var tab = File.ReadAllBytes(path);
            return tab;
        }

        private async Task<Tuple<bool, IEnumerable<string>>> Communicate(string algorithmName, long verticesCount, byte[] data)
        {
            var hubConn = new HubConnection(_serverUri);
            var hubProxy = hubConn.CreateHubProxy(_hubName);
            var res = new Tuple<bool, IEnumerable<string>>(false, null);
            var locker = new AutoResetEvent(false);
            var additionalData = data.LongLength > _dataChunk;
            var byteList = data.ToList();

            hubProxy.On("Result", (bool result, IEnumerable<string> msgList) =>
            {
                res = new Tuple<bool, IEnumerable<string>>(result, msgList);
                locker.Set();
            });
            
            hubConn.Start().Wait();
            
            var partialData = data.LongLength > _dataChunk ? byteList.GetRange(0, _dataChunk).ToArray() : data;

            var pos = await hubProxy.Invoke<long>("Compute", algorithmName, verticesCount, partialData, additionalData);

            var i = 1;

            while (additionalData)
            {
                var dataLeft = data.LongLength - i*_dataChunk;
                var chunkLength = dataLeft > _dataChunk ? _dataChunk : dataLeft;
                partialData = byteList.GetRange(i * _dataChunk, (int)chunkLength).ToArray();
                ++i;
                additionalData = (i * _dataChunk) < data.LongLength;

                await hubProxy.Invoke("AdditionalData", partialData, additionalData);
            }

            await Task.Run(() => locker.WaitOne());

            hubConn.Stop();

            return res;
        }
    }
}

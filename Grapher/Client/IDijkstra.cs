﻿using System.Collections.Generic;

namespace Client
{
    public interface IDijkstra : IGraph
    {
        long Root { get; }

        long? Destination { get; }

        void SaveResults(Dictionary<long, double?> result);

        void SaveResults(double? result);
    }
}

﻿using System.Collections.Generic;

namespace Client
{
    public interface IBFS : IGraph
    {
        long Root { get; }

        long? Destination { get; }

        bool PreVertexVisit(long vertexId);

        bool PostVertexVisit(long vertexId);

        void SaveResults(Dictionary<long, long?> results);

        void SaveResults(long? result);
    }
}

﻿using System.Web.Mvc;

namespace Server.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Algorithm(string algorithmName)
        {
            if (string.IsNullOrEmpty(algorithmName))
                return RedirectToAction("Index");

            if (algorithmName == "minimumspanningtree")
                ViewBag.Name = "Minimum spanning tree";
            else if(algorithmName == "dijkstra")
                ViewBag.Name = "Dijsktra";
            else if (algorithmName == "bfs")
                ViewBag.Name = "BFS";
            else
                return RedirectToAction("Index");

            return View((object)algorithmName);
        }
    }
}
﻿using System.Web.Configuration;
using StackExchange.Redis;

namespace Server.Hubs
{
    class Redis
    {
        private static readonly ConnectionMultiplexer Multiplexer;

        static Redis()
        {
            var conn = WebConfigurationManager.ConnectionStrings["Redis"].ConnectionString;

            Multiplexer = ConnectionMultiplexer.Connect(conn);
        }

        public static IDatabase GetDatabase()
        {
            return Multiplexer.GetDatabase();
        }
    }
}
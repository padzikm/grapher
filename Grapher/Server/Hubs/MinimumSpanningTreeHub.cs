﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using StackExchange.Redis;

namespace Server.Hubs
{
    public class MinimumSpanningTreeHub : Hub
    {
        private const string WorkingNodesKey = "WorkingNodes";
        private const string MstResultsKey = "MstResults";
        private const string MstCurrentMinKey = "MstCurrentMin";
        private const string MstNodesRoundKey = "MstNodesRound";
        private const string MstNodesFinishedKey = "MstNodesFinished";
        private readonly IDatabase _database;

        public MinimumSpanningTreeHub()
        {
            _database = Redis.GetDatabase();
        }

        public async Task UpdateMin(long vertexFrom, long vertexTo, double weight)
        {
            var transaction = _database.CreateTransaction();

            var key = string.Format("{0}_{1}", vertexFrom, vertexTo);

            if (vertexTo >= 0)
                transaction.SortedSetAddAsync(MstCurrentMinKey, key, weight);

            var roundNodesCountTask = transaction.ListRightPushAsync(MstNodesRoundKey, Context.ConnectionId);
            var finishedNodesCountTask = transaction.ListLengthAsync(MstNodesFinishedKey);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);
            
            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;
            
            if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await RoundHandler();
        }

        public async Task Result(bool success)
        {
            if (!success)
            {
                Abort();
                await CleanDatabase();
                return;
            }

            var transaction = _database.CreateTransaction();

            var roundNodesCountTask = transaction.ListLengthAsync(MstNodesRoundKey);
            var finishedNodesCountTask = transaction.ListRightPushAsync(MstNodesFinishedKey, Context.ConnectionId);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;

            if(finishedNodesCount == workingNodesCount) 
            {
                var ids = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(ids);
                await CleanDatabase();
            }
            else if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await RoundHandler();
        }

        public async Task CleanAll()
        {
            await CleanDatabase();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var transaction = _database.CreateTransaction();

            transaction.ListRemoveAsync(MstNodesRoundKey, Context.ConnectionId);
            transaction.ListRemoveAsync(MstNodesFinishedKey, Context.ConnectionId);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            transaction.Execute();

            var workingNodesCount = workingNodesCountTask.Result;

            if(workingNodesCount == 0)
                CleanDatabase();

            return base.OnDisconnected(stopCalled);
        }

        private async Task RoundHandler()
        {
            var elems = _database.SortedSetRangeByRankWithScores(MstCurrentMinKey, 0, 0);

            if (elems.Length == 0)
            {
                var res = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(res);
                await CleanDatabase();
                return;
            }

            var elem = elems.First();
            var str = (string)elem.Element;
            var index = str.IndexOf("_");
            var vertFrom = long.Parse(str.Substring(0, index));
            var vertTo = long.Parse(str.Substring(index + 1));
            var score = (double)elem.Score;
            var ids = _database.ListRange(MstNodesRoundKey).Select(p => p.ToString()).ToList();

            _database.KeyDelete(MstCurrentMinKey);
            _database.KeyDelete(MstNodesRoundKey);
            _database.HashSet(MstResultsKey, vertTo, vertFrom);

            BroadcastMin(ids, vertFrom, vertTo, score);
        }

        private void BroadcastMin(IList<string> ids, long vertexFrom, long vertexTo, double weight)
        {
            Clients.Clients(ids).UpdateGraph(vertexFrom, vertexTo, weight);
        }

        private void SaveResults(IList<string> ids)
        {
            Clients.Clients(ids).SaveResults();
        }

        private void Abort()
        {
            var graphHub = new GraphHub();
            graphHub.Abort();
        }

        private async Task CleanDatabase()
        {
            var transaction = _database.CreateTransaction();

            transaction.KeyDeleteAsync(MstResultsKey);
            transaction.KeyDeleteAsync(MstCurrentMinKey);
            transaction.KeyDeleteAsync(MstNodesRoundKey);
            transaction.KeyDeleteAsync(MstNodesFinishedKey);

            await transaction.ExecuteAsync();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using StackExchange.Redis;

namespace Server.Hubs
{
    public class DijkstraHub : Hub
    {
        private const string WorkingNodesKey = "WorkingNodes";
        private const string DijkstraResultsKey = "DijkstraResults";
        private const string DijkstraCurrentMinKey = "DijkstraCurrentMin";
        private const string DijkstraNodesRoundKey = "DijkstraNodesRound";
        private const string DijkstraNodesFinishedKey = "DijkstraNodesFinished";

        private readonly IDatabase _database;

        public DijkstraHub()
        {
            _database = Redis.GetDatabase();
        }

        public async Task UpdateMin(long vertexFrom, long vertexTo, double route)
        {
            var transaction = _database.CreateTransaction();

            var key = string.Format("{0}_{1}", vertexFrom, vertexTo);

            if (vertexTo >= 0)
                transaction.SortedSetAddAsync(DijkstraCurrentMinKey, key, route);

            var roundNodesCountTask = transaction.ListRightPushAsync(DijkstraNodesRoundKey, Context.ConnectionId);
            var finishedNodesCountTask = transaction.ListLengthAsync(DijkstraNodesFinishedKey);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;

            if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await HandleRound();
        }

        public async Task Result(bool success)
        {
            if (!success)
            {
                Abort();
                await CleanDatabase();
                return;
            }

            var transaction = _database.CreateTransaction();

            var roundNodesCountTask = transaction.ListLengthAsync(DijkstraNodesRoundKey);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);
            var finishedNodesCountTask = transaction.ListRightPushAsync(DijkstraNodesFinishedKey, Context.ConnectionId);

            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;

            if (finishedNodesCount == workingNodesCount)
            {
                var ids = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(ids);
                await CleanDatabase();
            }
            else if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await HandleRound();
        }

        public async Task CleanAll()
        {
            await CleanDatabase();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var transaction = _database.CreateTransaction();

            transaction.ListRemoveAsync(DijkstraNodesRoundKey, Context.ConnectionId);
            transaction.ListRemoveAsync(DijkstraNodesFinishedKey, Context.ConnectionId);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            transaction.Execute();

            var workingNodesCount = workingNodesCountTask.Result;

            if (workingNodesCount == 0)
                CleanDatabase();

            return base.OnDisconnected(stopCalled);
        }

        private async Task HandleRound()
        {
            var elems = _database.SortedSetRangeByRankWithScores(DijkstraCurrentMinKey, 0, 0);

            if (elems.Length == 0)
            {
                var res = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(res);
                await CleanDatabase();
                return;
            }

            var elem = elems.First();
            var str = (string)elem.Element;
            var index = str.IndexOf("_");
            var vertFrom = long.Parse(str.Substring(0, index));
            var vertTo = long.Parse(str.Substring(index + 1));
            var score = (double)elem.Score;
            var ids = _database.ListRange(DijkstraNodesRoundKey).Select(p => p.ToString()).ToList();

            _database.KeyDelete(DijkstraCurrentMinKey);
            _database.KeyDelete(DijkstraNodesRoundKey);
            _database.HashSet(DijkstraResultsKey, vertTo, score);

            BroadcastMin(ids, vertTo, score);
        }

        private void BroadcastMin(IList<string> ids, long vertex, double route)
        {
            Clients.Clients(ids).UpdateGraph(vertex, route);
        }

        private void SaveResults(IList<string> ids)
        {
            Clients.Clients(ids).SaveResults();
        }

        private void Abort()
        {
            var graphHub = new GraphHub();
            graphHub.Abort();
        }

        private async Task CleanDatabase()
        {
            var transaction = _database.CreateTransaction();

            transaction.KeyDeleteAsync(DijkstraResultsKey);
            transaction.KeyDeleteAsync(DijkstraCurrentMinKey);
            transaction.KeyDeleteAsync(DijkstraNodesRoundKey);
            transaction.KeyDeleteAsync(DijkstraNodesFinishedKey);

            await transaction.ExecuteAsync();
        }
    }
}
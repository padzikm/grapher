﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using StackExchange.Redis;

namespace Server.Hubs
{
    public class BFSHub : Hub
    {
        private const string WorkingNodesKey = "WorkingNodes";
        private const string BfsResultsKey = "BfsResults";
        private const string BfsComputedDistanceKey = "BfsCurrentDistance";
        private const string BfsFoundRoundKey = "BfsFoundRound";
        private const string BfsDestFoundRoundKey = "BfsDestFoundRound";
        private const string BfsNodesRoundKey = "BfsNodesRound";
        private const string BfsNodesFinishedKey = "BfsNodesFinished";

        private readonly IDatabase _database;

        public BFSHub()
        {
            _database = Redis.GetDatabase();
        }

        public async Task Barrier(bool found, bool destinationFound)
        {
            var transaction = _database.CreateTransaction();

            if (found)
                transaction.StringIncrementAsync(BfsFoundRoundKey);

            if (destinationFound)
                transaction.StringSetAsync(BfsDestFoundRoundKey, 1);

            var roundNodesCountTask = transaction.ListRightPushAsync(BfsNodesRoundKey, Context.ConnectionId);
            var finishedNodesCountTask = transaction.ListLengthAsync(BfsNodesFinishedKey);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;

            if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await RoundHandler();
        }

        public async Task Result(bool success)
        {
            if (!success)
            {
                Abort();
                await CleanDatabase();
                return;
            }

            var transaction = _database.CreateTransaction();

            var roundNodesCountTask = transaction.ListLengthAsync(BfsNodesRoundKey);
            var finishedNodesCountTask = transaction.ListRightPushAsync(BfsNodesFinishedKey, Context.ConnectionId);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            await transaction.ExecuteAsync();

            var roundNodesCount = await roundNodesCountTask;
            var finishedNodesCount = await finishedNodesCountTask;
            var workingNodesCount = await workingNodesCountTask;

            if (finishedNodesCount == workingNodesCount)
            {
                var ids = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(ids);
                await CleanDatabase();
            }
            else if (roundNodesCount + finishedNodesCount == workingNodesCount)
                await RoundHandler();
        }

        public async Task CleanAll()
        {
            await CleanDatabase();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var transaction = _database.CreateTransaction();

            transaction.ListRemoveAsync(BfsNodesFinishedKey, Context.ConnectionId);
            transaction.ListRemoveAsync(BfsNodesRoundKey, Context.ConnectionId);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            transaction.Execute();

            var workingNodesCount = workingNodesCountTask.Result;

            if (workingNodesCount == 0)
                CleanDatabase();

            return base.OnDisconnected(stopCalled);
        }

        private async Task RoundHandler()
        {
            var foundCount = (long)_database.StringGetSet(BfsFoundRoundKey, 0);
            var destFound = (long)_database.StringGet(BfsDestFoundRoundKey);
            var dist = _database.StringIncrement(BfsComputedDistanceKey);

            if (foundCount == 0 || destFound == 1)
            {
                var res = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();
                SaveResults(res);
                await CleanDatabase();
                return;
            }

            var ids = _database.ListRange(BfsNodesRoundKey).Select(p => p.ToString()).ToList();

            _database.KeyDelete(BfsNodesRoundKey);

            BroadcastDistance(ids, dist);
        }

        private void BroadcastDistance(IList<string> ids, long distance)
        {
            Clients.Clients(ids).UpdateGraph(distance);
        }

        private void SaveResults(IList<string> ids)
        {
            Clients.Clients(ids).SaveResults();
        }

        private void Abort()
        {
            var graphHub = new GraphHub();
            graphHub.Abort();
        }

        private async Task CleanDatabase()
        {
            var transaction = _database.CreateTransaction();

            transaction.KeyDeleteAsync(BfsComputedDistanceKey);
            transaction.KeyDeleteAsync(BfsResultsKey);
            transaction.KeyDeleteAsync(BfsFoundRoundKey);
            transaction.KeyDeleteAsync(BfsDestFoundRoundKey);
            transaction.KeyDeleteAsync(BfsNodesRoundKey);
            transaction.KeyDeleteAsync(BfsNodesFinishedKey);

            await transaction.ExecuteAsync();
        }
    }
}
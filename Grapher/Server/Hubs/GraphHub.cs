﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Hosting;
using Microsoft.AspNet.SignalR;
using StackExchange.Redis;

namespace Server.Hubs
{
    public class GraphHub : Hub
    {
        private const string AllNodesKey = "AllNodes";
        private const string WorkingNodesKey = "WorkingNodes";
        private const string BackupNodesKey = "BackupNodes";
        private const string ClientsKey = "Clients";
        private const string ClientsDataSendKey = "ClientsDataSend";
        private const string CurrentClientKey = "CurrentClient";
        private const string AlgorithmNamesKey = "AlgorithmNames";
        private const string AlgorithmVerticesCountKey = "AlgorithmVerticesCount";
        private const string AlgorithmResultsKey = "AlgorithmResults";
        private const string AlgorithmResultsMessagesKey = "AlgorithmResultsMessages";
        private const string GraphSplitKey = "GraphSplit";

        private readonly IDatabase _database;

        public GraphHub()
        {
            _database = Redis.GetDatabase();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var node = _database.ListRemove(AllNodesKey, Context.ConnectionId);

            if (node > 0)
            {
                var working = _database.ListRemove(WorkingNodesKey, Context.ConnectionId);
                _database.ListRemove(BackupNodesKey, Context.ConnectionId);

                if (working > 0)
                    if (_database.ListLength(BackupNodesKey) > 0)
                        SendBackup(Context.ConnectionId);
                    else
                    {
                        Result(false, "Not enough backup nodes");
                        Abort();
                    }
            }
            else
            {
                if (_database.StringGet(CurrentClientKey) == Context.ConnectionId)
                {
                    CleanClientData(Context.ConnectionId, true).Wait();
                    Abort();
                }
                else
                    CleanClientData(Context.ConnectionId).Wait();
            }

            return base.OnDisconnected(stopCalled);
        }

        public async Task<bool> Register()
        {
            var transaction = _database.CreateTransaction();
            
            var allNodesCountTask = transaction.ListRightPushAsync(AllNodesKey, Context.ConnectionId);
            var clientTask = transaction.StringGetAsync(CurrentClientKey);

            await transaction.ExecuteAsync();

            var allNodesCount = await allNodesCountTask;
            var client = await clientTask;

            if (!client.IsNull)
            {
                if(allNodesCount == 1)
                    RunNextClient();
                else
                    _database.ListRightPush(BackupNodesKey, Context.ConnectionId);
            }

            return true;
        }

        public async Task<long> Compute(string algorithmName, long verticesCount, byte[] data, bool additionalData)
        {
            string targetPath = GetClientDataPath(Context.ConnectionId);
            File.WriteAllBytes(targetPath, data);

            var transaction = _database.CreateTransaction();

            transaction.HashSetAsync(AlgorithmNamesKey, Context.ConnectionId, algorithmName);
            transaction.HashSetAsync(AlgorithmVerticesCountKey, Context.ConnectionId, verticesCount);
            transaction.HashSetAsync(ClientsDataSendKey, Context.ConnectionId, !additionalData);
            var clientsCountTask = transaction.ListRightPushAsync(ClientsKey, Context.ConnectionId);

            await transaction.ExecuteAsync();

            var clientsCount = await clientsCountTask;

            if (clientsCount == 1 && !additionalData)
            {
                _database.StringSet(CurrentClientKey, Context.ConnectionId);
                StartComputing(algorithmName, verticesCount, data);
            }

            return clientsCount;
        }

        public async Task AdditionalData(byte[] data, bool additionalData)
        {
            string targetPath = GetClientDataPath(Context.ConnectionId);

            var bytes = File.ReadAllBytes(targetPath).ToList();

            bytes.AddRange(data);

            File.WriteAllBytes(targetPath, bytes.ToArray());

            if (!additionalData)
            {
                _database.HashSet(ClientsDataSendKey, Context.ConnectionId, true);

                if (_database.ListGetByIndex(ClientsKey, 0) == Context.ConnectionId)
                {
                    var algorithmName = _database.HashGet(AlgorithmNamesKey, Context.ConnectionId);
                    var verticesCount = (long) _database.HashGet(AlgorithmVerticesCountKey, Context.ConnectionId);
                    _database.StringSet(CurrentClientKey, Context.ConnectionId);

                    StartComputing(algorithmName, verticesCount, bytes.ToArray());
                }
            }
        }

        public async Task Result(bool success, string message)
        {
            var transaction = _database.CreateTransaction();

            transaction.HashSetAsync(AlgorithmResultsKey, Context.ConnectionId, success);
            transaction.HashSetAsync(AlgorithmResultsMessagesKey, Context.ConnectionId, message);
            var algorithmResultsCountTask = transaction.HashLengthAsync(AlgorithmResultsKey);
            var workingNodesCountTask = transaction.ListLengthAsync(WorkingNodesKey);

            await transaction.ExecuteAsync();

            var algorithmResultsCount = await algorithmResultsCountTask;
            var workingNodesCount = await workingNodesCountTask;

            if (workingNodesCount <= algorithmResultsCount)
            {
                await SendComputeResult();

                await CleanNodesData();

                await RunNextClient();
            }
        }

        internal void Abort()
        {
            var nodes = _database.ListRange(WorkingNodesKey).Select(p => p.ToString()).ToList();

            if (Context != null)
                Clients.Clients(nodes).Abort();
            else
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<GraphHub>();
                context.Clients.Clients(nodes).Abort();
            }
        }

        private async Task RunNextClient()
        {
            var nextClient = _database.ListGetByIndex(ClientsKey, 0);

            if (nextClient.IsNull)
                return;

            var nextClientsData = (bool)_database.HashGet(ClientsDataSendKey, nextClient);

            if (!nextClientsData)
                return;

            _database.StringSet(CurrentClientKey, nextClient);
            var targetPath = GetClientDataPath(nextClient);
            var data = File.ReadAllBytes(targetPath);

            var algorithmName = _database.HashGet(AlgorithmNamesKey, nextClient);
            var verticesCount = (long)_database.HashGet(AlgorithmVerticesCountKey, nextClient);

            StartComputing(algorithmName, verticesCount, data);
        }

        private void StartComputing(string algorithmName, long verticesCount, byte[] data)
        {
            var allNodesCount = (long)_database.ListLength(AllNodesKey);
            var backupNodesCount = (long)(allNodesCount > 1 ? Math.Ceiling(0.1 * allNodesCount) : 0);
            var workingNodesCount = (long)(allNodesCount - backupNodesCount);

            var workingNodes = _database.ListRange(AllNodesKey, 0L, workingNodesCount - 1);
            var backupNodes = _database.ListRange(AllNodesKey, workingNodesCount);
            _database.ListRightPush(BackupNodesKey, backupNodes);

            var nodesIds = workingNodes.Select(p => p.ToString()).ToList();

            for (int i = 0; i < workingNodesCount; ++i)
            {
                var tuple = DivideGraph(verticesCount, workingNodesCount, i);

                if (tuple != null)
                {
                    var split = string.Format("{0}_{1}_{2}_{3}", tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4);
                    _database.HashSet(GraphSplitKey, nodesIds[i], split);

                    Clients.Client(nodesIds[i]).Compute(algorithmName, tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4, data, false);

                    _database.ListRightPush(WorkingNodesKey, nodesIds[i]);
                }
                else
                    _database.ListRightPush(BackupNodesKey, nodesIds[i]);
            }
        }

        private Tuple<long, long, long, long> DivideGraph(long verticesCount, long nodesCount, long nodeNumber)
        {
            var nodesInLineCount = Math.Floor(Math.Sqrt(nodesCount));

            if (nodesInLineCount == 1)
            {
                var width1 = verticesCount / nodesCount;
                var fromOut1 = 0;
                var toOut1 = verticesCount - 1;
                var fromIn1 = width1 * nodeNumber;
                var toIn1 = nodeNumber == nodesCount - 1 ? verticesCount - 1 : width1 * (nodeNumber + 1) - 1;

                return new Tuple<long, long, long, long>(fromOut1, toOut1, fromIn1, toIn1);
            }

            if (nodesInLineCount * nodesInLineCount <= nodeNumber)
                return null;

            var lineNr = Math.Floor(nodeNumber / nodesInLineCount);
            var posInLine = nodeNumber - lineNr * nodesInLineCount;
            var width = Math.Floor(verticesCount / nodesInLineCount);
            var fromOut = (long)(lineNr * width);
            var toOut = (long)(lineNr < nodesInLineCount - 1 ? (lineNr + 1) * width - 1 : verticesCount - 1);
            var fromIn = (long)(posInLine * width);
            var toIn = (long)(posInLine < nodesInLineCount - 1 ? (posInLine + 1) * width - 1 : verticesCount - 1);

            return new Tuple<long, long, long, long>(fromOut, toOut, fromIn, toIn);
        }

        private void SendBackup(string failedNodeId)
        {
            var client = _database.StringGet(CurrentClientKey);
            var isFinished = _database.HashGet(AlgorithmResultsKey, failedNodeId);

            if (client.IsNull || !isFinished.IsNull)
                return;

            var targetPath = GetClientDataPath(client);
            var data = File.ReadAllBytes(targetPath);
            var algorithmName = _database.HashGet(AlgorithmNamesKey, client);

            var backup = _database.ListLeftPop(BackupNodesKey);
            _database.ListRightPush(WorkingNodesKey, backup);

            var tmp = _database.HashGet(GraphSplitKey, failedNodeId).ToString();
            var graphSplit = tmp.Split('_');
            var outFrom = graphSplit[0];
            var outTo = graphSplit[1];
            var inFrom = graphSplit[2];
            var inTo = graphSplit[3];
            _database.HashDelete(GraphSplitKey, failedNodeId);
            _database.HashSet(GraphSplitKey, backup, tmp);

            Clients.Client(backup).Compute(algorithmName, outFrom, outTo, inFrom, inTo, data, true);
        }

        private async Task SendComputeResult()
        {
            var currentClient = _database.StringGet(CurrentClientKey);

            if (!currentClient.IsNull)
            {
                var algorithmResults = _database.HashValues(AlgorithmResultsKey).ToList();

                var result = algorithmResults.Count > 0 && algorithmResults.All(p => p == true);

                var msgList = _database.HashValues(AlgorithmResultsMessagesKey).ToList().Where(p => !p.IsNullOrEmpty);

                Clients.Client(currentClient).Result(result, msgList);

                await CleanClientData(currentClient, true);
            }
        }

        private async Task CleanNodesData()
        {
            var transaction = _database.CreateTransaction();

            transaction.KeyDeleteAsync(AlgorithmResultsKey);
            transaction.KeyDeleteAsync(AlgorithmResultsMessagesKey);
            transaction.KeyDeleteAsync(WorkingNodesKey);
            transaction.KeyDeleteAsync(BackupNodesKey);
            transaction.KeyDeleteAsync(GraphSplitKey);

            await transaction.ExecuteAsync();
        }

        private async Task CleanClientData(string clientId, bool currentClient = false)
        {
            var transaction = _database.CreateTransaction();

            transaction.HashDeleteAsync(AlgorithmNamesKey, clientId);
            transaction.HashDeleteAsync(AlgorithmVerticesCountKey, clientId);
            transaction.HashDeleteAsync(ClientsDataSendKey, clientId);
            transaction.ListRemoveAsync(ClientsKey, clientId);

            if (currentClient)
                transaction.KeyDeleteAsync(CurrentClientKey);

            await transaction.ExecuteAsync();

            var path = GetClientDataPath(clientId);
            File.Delete(path);
        }

        private string GetClientDataPath(string clientId)
        {
            if (string.IsNullOrEmpty(clientId))
                return null;

            var archiveName = clientId + ".7z";
            string targetFolder = HostingEnvironment.MapPath("~/App_Data");
            string targetPath = Path.Combine(targetFolder, archiveName);
            return targetPath;
        }
    }
}
